import { createStore, applyMiddleware } from 'redux';
import mainReducer from './storeReducer';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

let store = createStore(
    mainReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

export default store;
