import { LOAD_GAMES, LOAD_USERS } from './actions';

export const loadGames = (games) => ({
    type: LOAD_GAMES,
    payload: games,
});

export const loadUsers = (users) => ({
    type: LOAD_USERS,
    payload: users,
});
