import { LOAD_GAMES, LOAD_USERS } from './actions';

const initialState = {
    games: [],
    users: [],
};

export default function mainReducer(store = initialState, action) {
    switch (action.type) {
        case LOAD_GAMES:
            return {
                ...store,
                games: action.payload,
            };
        case LOAD_USERS:
            return {
                ...store,
                users: action.payload,
            };
        default:
            return store;
    }
}
