import Cookies from 'js-cookie';

const API = {
    apiBase: 'https://uaky.herokuapp.com/api/v1/',
    isRefreshing: false,

    async createRequest(endpoint, method, headers, data, isFormData = false) {
        const options = this.createOptions(method, headers, data, isFormData);

        const request = fetch(this.apiBase + endpoint, options);

        return request.then((error) => {
            if (error?.status === 403) {
                if (!this.isRefreshing) {
                    this.isRefreshing = true;
                    return this.refreshToken().then((res) => {
                        if (res?.status === 200) {
                            return res.json().then((tokens) => {
                                Cookies.set('accessToken', tokens.accessToken);
                                Cookies.set(
                                    'refreshToken',
                                    tokens.refreshToken
                                );
                                this.isRefreshing = false;
                                const newHeaders = headers;
                                newHeaders.Authorization = `Bearer ${tokens.accessToken}`;
                                const newOptions = this.createOptions(
                                    method,
                                    newHeaders,
                                    data,
                                    isFormData
                                );
                                return fetch(
                                    this.apiBase + endpoint,
                                    newOptions
                                );
                            });
                        }
                    });
                } else {
                    return new Promise((resolve) => {
                        const intervalId = setInterval(() => {
                            if (!this.isRefreshing) {
                                clearInterval(intervalId);
                                const newHeaders = headers;
                                newHeaders.Authorization = `Bearer ${Cookies.get(
                                    'accessToken'
                                )}`;
                                const newOptions = this.createOptions(
                                    method,
                                    newHeaders,
                                    data,
                                    isFormData
                                );
                                resolve(
                                    fetch(this.apiBase + endpoint, newOptions)
                                );
                            }
                        }, 100);
                    });
                }
            }
            return request;
        });
    },

    async refreshToken() {
        return await this.createRequest('auth/refresh', 'POST', {
            Authorization: `Bearer ${Cookies.get('refreshToken')}`,
        });
    },

    createOptions(method, headers, data, isFormData) {
        const options = {};
        if (headers) options.headers = headers;
        if (data && !isFormData) options.body = JSON.stringify(data);
        if (data && isFormData) options.body = data;
        options.method = method;
        return options;
    },

    async getGames() {
        return await this.createRequest('game', 'GET');
    },

    async commonSearchUsers(id) {
        return await this.createRequest(`common-search/${id}`, 'GET', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async onlineSearchUsers(id) {
        return await this.createRequest(`online-search/${id}`, 'GET', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async refreshUsers(id) {
        return await this.createRequest(`common-search/${id}/views`, 'DELETE', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async onlineRefreshUsers(id) {
        return await this.createRequest(`online-search/${id}/views`, 'DELETE', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async registration(data) {
        return await this.createRequest(
            'register',
            'POST',
            { Accept: '*/*', 'Content-Type': 'application/json' },
            data
        );
    },

    async login(data) {
        return await this.createRequest(
            'auth/login',
            'POST',
            { Accept: '*/*', 'Content-Type': 'application/json' },
            data
        );
    },

    async getUserInfo() {
        return await this.createRequest('me', 'GET', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async updateUserGame(game_id, data) {
        return await this.createRequest(
            `me/games/${game_id}`,
            'PUT',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            data
        );
    },

    async deleteUserGame(game_id) {
        return await this.createRequest(`me/games/${game_id}`, 'DELETE', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async addUserGame(data) {
        return await this.createRequest(
            'me/games',
            'POST',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            data
        );
    },

    async editNickname(data) {
        return await this.createRequest(
            'me/nickname',
            'PATCH',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            data
        );
    },

    async editPassword(data) {
        return await this.createRequest(
            'me/password',
            'PATCH',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            data
        );
    },

    async editUserPicture(data) {
        return await this.createRequest(
            'me/image',
            'PATCH',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
                'Content-Type': 'application/json',
            },
            data
        );
    },

    async uploadPicture(payload) {
        return await this.createRequest(
            'images/users',
            'POST',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
            },
            payload,
            true
        );
    },

    async getUserLikes() {
        return await this.createRequest('me/likes', 'GET', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
        });
    },

    async likeUser(userId, gameId) {
        return await this.createRequest(
            `users/${userId}/likes/${gameId}`,
            'POST',
            {
                Authorization: `Bearer ${Cookies.get('accessToken')}`,
                Accept: '*/*',
                'Content-Type': 'application/json',
            }
        );
    },

    async keepOnline() {
        return await this.createRequest('me/last-visited-date', 'PATCH', {
            Authorization: `Bearer ${Cookies.get('accessToken')}`,
            Accept: '*/*',
            'Content-Type': 'application/json',
        });
    },
};

export default API;
