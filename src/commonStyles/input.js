import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';

export const Input = styled.input`
    margin: 8px 0;
    transition: 0.4s;
    caret-color: #94ae3f;
    background-color: #243754;
    border: 0;
    padding: 15px;
    border: 1px solid transparent;
    color: white;
    padding-left: 20px;
    border-radius: 5px;

    &:focus {
        background-color: #314869;
        outline: none;
        border: 1px solid #94ae3f;
    }

    &::placeholder {
        color: rgba(255, 255, 255, 0.253);
    }
`;

export const ErrorWrapper = styled.div`
    color: tomato;
    display: flex;
    flex-direction: column;
    padding: 5px;
`;

export const ErrorMessage = styled.span`
    margin-bottom: 15px;
`;

export const Error = ({ errors }) => {
    return (
        <ErrorWrapper>
            {errors?.map((error) => (
                <ErrorMessage key={uuid()}>{error}</ErrorMessage>
            ))}
        </ErrorWrapper>
    );
};

Error.propTypes = {
    errors: PropTypes.any,
};
