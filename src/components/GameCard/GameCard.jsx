import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledUserGameCard = styled.div`
    transition: 0.4s;
    margin: 15px;
    width: 250px;
    height: 400px;
    background-color: #1d2633;
    font-size: 22px;

    &:hover {
        -webkit-box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
    }

    @media screen and (max-width: 430px) {
        height: auto;
        width: 80vw;
        padding: 10px;
    }
`;

const GameImage = styled.div`
    position: relative;
    background-image: url(${(props) => props.image});
    background-size: cover;
    width: 100%;
    height: 350px;

    @media screen and (max-width: 430px) {
        background-image: url(${(props) => props.smallImage});
        height: 40px;
    }
`;

const GameIcon = styled.div`
    background-image: url(${(props) => props.icon});
    background-size: cover;
    height: 30px;
    width: 30px;
    margin-right: 5px;
`;

const GameTitle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    text-align: center;
    font-weight: bold;
    margin-top: 10px;
`;

export default function GameCard({ gameTitle, imageLarge, smallImage, icon }) {
    return (
        <StyledUserGameCard>
            <GameImage image={imageLarge} smallImage={smallImage} />

            <GameTitle>
                <GameIcon icon={icon} />
                {gameTitle}
            </GameTitle>
        </StyledUserGameCard>
    );
}

GameCard.propTypes = {
    gameTitle: PropTypes.string,
    imageLarge: PropTypes.string,
    icon: PropTypes.string,
    smallImage: PropTypes.string,
};
