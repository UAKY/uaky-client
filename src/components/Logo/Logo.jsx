import React from 'react';
import PropTypes from 'prop-types';

import styles from './Logo.module.css';

Logo.propTypes = {
    size: PropTypes.string,
};

function Logo({ size }) {
    return (
        <img
            width={size}
            height={size}
            className={styles.Logo}
            src="https://cdn.logo.com/hotlink-ok/logo-social-sq.png"
            alt="Logo"
        />
    );
}

export default Logo;
