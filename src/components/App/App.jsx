import React, { useEffect } from 'react';
import MainContainer from '../MainContainer/MainContainer';
import { Route, Switch, useLocation, Redirect } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';

import Header from '../Header';
import Footer from '../Footer';
import Login from '../Login';

import loginBG from '../../assets/images/loginBG.jpg';
import patternBG from '../../assets/images/pattern.png';
import pattern1BG from '../../assets/images/pattern1.png';
import homeBG from '../../assets/images/homeBg.jpg';
import registerBG from '../../assets/images/registerBG.jpg';
import Registration from '../Registration';
import loadData from './connectStore';
import PropTypes from 'prop-types';
import SearchTable from '../SearchTable';
import SearchChoose from '../SearchChoose';
import Search from '../Search';
import Profile from '../Profile/Profile';
import UserLikes from '../UserLikes';
import API from '../../api/apiClient';
import OnlineSearch from '../Search/OnlineSearch';
import HomePage from '../HomePage';

const AuthorizedRoute = ({ path, children }) => {
    return (
        <Route path={path}>
            {Cookies.get('accessToken') ? children : <Redirect to="/login" />}
        </Route>
    );
};

const UnAuthorizedRoute = ({ path, children }) => {
    return (
        <Route path={path}>
            {Cookies.get('accessToken') ? <Redirect to="/home" /> : children}
        </Route>
    );
};

UnAuthorizedRoute.propTypes = {
    path: PropTypes.string,
    children: PropTypes.node,
};

AuthorizedRoute.propTypes = {
    path: PropTypes.string,
    children: PropTypes.node,
};

function App(props) {
    const location = useLocation();

    const startTimer = () => {
        if (Cookies.get('accessToken')) {
            API.keepOnline();
            setInterval(() => {
                API.keepOnline();
            }, 1000 * 60 * 4);
        }
    };

    useEffect(() => {
        props.loadData();
        startTimer();
    }, []);

    return (
        <div className="appWrapper">
            <Header />
            <TransitionGroup>
                <CSSTransition
                    key={location.key}
                    classNames="fade"
                    timeout={300}
                >
                    <Switch location={location}>
                        <Route path="/home" exact>
                            <MainContainer bg={homeBG}>
                                <HomePage />
                            </MainContainer>
                        </Route>

                        <AuthorizedRoute path="/search" exact>
                            <MainContainer isCentered={false} bg={pattern1BG}>
                                <SearchTable />
                            </MainContainer>
                        </AuthorizedRoute>

                        <AuthorizedRoute path="/search/:id" exact>
                            <MainContainer bg={pattern1BG}>
                                <SearchChoose />
                            </MainContainer>
                        </AuthorizedRoute>

                        <AuthorizedRoute path="/me" exact>
                            <MainContainer bg={pattern1BG}>
                                <Profile />
                            </MainContainer>
                        </AuthorizedRoute>

                        <AuthorizedRoute path="/search/:id/common" exact>
                            <MainContainer bg={patternBG}>
                                <Search />
                            </MainContainer>
                        </AuthorizedRoute>

                        <AuthorizedRoute path="/search/:id/online" exact>
                            <MainContainer bg={patternBG}>
                                <OnlineSearch />
                            </MainContainer>
                        </AuthorizedRoute>

                        <AuthorizedRoute path="/likes" exact>
                            <MainContainer bg={pattern1BG}>
                                <UserLikes />
                            </MainContainer>
                        </AuthorizedRoute>

                        <UnAuthorizedRoute path="/login" exact>
                            <MainContainer bg={loginBG}>
                                <Login />
                            </MainContainer>
                        </UnAuthorizedRoute>

                        <UnAuthorizedRoute path="/registration" exact>
                            <MainContainer bg={registerBG}>
                                <Registration />
                            </MainContainer>
                        </UnAuthorizedRoute>

                        <Route path="/">
                            <Redirect to="/home" />
                        </Route>
                    </Switch>
                </CSSTransition>
            </TransitionGroup>
            <Footer />
        </div>
    );
}

const mapDispatchToProps = {
    loadData,
};

App.propTypes = {
    loadData: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(App);
