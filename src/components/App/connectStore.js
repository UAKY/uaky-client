import API from '../../api/apiClient';
import { loadGames } from '../../redux/actionsCreators';

const loadData = () => {
    return (dispatch) => {
        API.getGames().then((res) => {
            res.json().then((games) => {
                dispatch(loadGames(games));
            });
        });
    };
};

export default loadData;
