import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledChangeAvatar = styled.label`
    position: absolute;
    right: -5px;
    bottom: -5px;
    width: 40px;
    height: 40px;
    display: grid;
    place-items: center;
    border-radius: 50%;
    padding: 5px;
    background-color: #94ae3f;
    border: 10px solid #1d2633;
    color: #1d2633;
    font-size: 30px;
    cursor: pointer;
`;

export default function ChangeAvatar({ id }) {
    return (
        <StyledChangeAvatar htmlFor={id}>
            <i className="fas fa-camera"></i>
        </StyledChangeAvatar>
    );
}

ChangeAvatar.propTypes = {
    id: PropTypes.any,
};
