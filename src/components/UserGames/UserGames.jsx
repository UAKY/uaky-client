import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import UserGameCard from '../UserGameCard';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { v4 as uuid } from 'uuid';
import Modal from '../Modal';
import { Input } from '../../commonStyles/input';
import Dropdown from '../Dropdown';
import API from '../../api/apiClient';
import { useHistory } from 'react-router';

const StyledUserGames = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    padding-top: 15px;
`;

const AddGame = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transition: 0.4s;
    margin: 15px;
    width: 250px;
    height: 400px;
    background-color: #1d2633;
    font-size: 22px;
    cursor: pointer;

    &:hover {
        -webkit-box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
        background-color: #2f4363;
    }
`;

const Plus = styled.i`
    color: #94ae3f;
    font-size: 70px;
`;

const Text = styled.span`
    margin-top: 20px;
    color: white;
    font-weight: bold;
`;

const ModalButton = () => (
    <AddGame>
        <Plus className="fas fa-plus" />
        <Text>Add game</Text>
    </AddGame>
);

const FormInput = styled(Input)`
    width: calc(100% - 40px);
    font-size: inherit;
    margin-bottom: 20px;
`;

const Label = styled.label`
    font-size: inherit;
    color: white;
    margin-bottom: 5px;
`;

const Margin = styled.div`
    width: 100%;
    height: 15px;
`;

function UserGames({ games, userGames }) {
    const history = useHistory();
    const dropdownPlaceholder = 'Select game';
    const [selectedGame, setSelectedGame] = useState(dropdownPlaceholder);
    const [gamesDifference, setGamesDifference] = useState([]);

    useEffect(() => {
        const diff = games.filter((game) => {
            return !userGames.some((userGame) => userGame.id === game.id);
        });

        setGamesDifference(diff);
    }, [userGames]);

    const addGame = () => {
        const link = document.getElementById('addGameLink').value;
        const description = document.getElementById('addGameDesc').value;

        const data = {
            id: gamesDifference[selectedGame].id,
            profileLink: link,
            description: description,
        };

        API.addUserGame(data).then(() => {
            history.go(0);
        });
    };

    const showId = (id) => {
        setSelectedGame(id);
    };

    const Content = (
        <>
            <Label htmlFor="addGameDropdown">Choose a game:</Label>
            <Dropdown
                id="addGameDropdown"
                selectId={showId}
                choices={gamesDifference}
                dropdownPlaceholder={dropdownPlaceholder}
            />
            <Margin />
            <Label htmlFor="addGameLink">Profile link:</Label>
            <FormInput id="addGameLink" />
            <Label htmlFor="addGameDesc">Description:</Label>
            <FormInput id="addGameDesc" />
        </>
    );

    return (
        <StyledUserGames>
            {userGames?.map((userGame) => {
                return (
                    <UserGameCard
                        key={uuid()}
                        userGame={userGame}
                        game={games.find((game) => game.id === userGame.id)}
                    />
                );
            })}

            {userGames.length !== games.length && (
                <Modal
                    modalButton={ModalButton}
                    Content={Content}
                    okButtonText="Add"
                    okclick={addGame}
                />
            )}
        </StyledUserGames>
    );
}

UserGames.propTypes = {
    games: PropTypes.array,
    userGames: PropTypes.array,
};

const mapStateToProps = (state) => {
    return {
        games: state.games,
    };
};

export default connect(mapStateToProps)(UserGames);
