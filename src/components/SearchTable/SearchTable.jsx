import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { v4 as uuid } from 'uuid';
import styles from './SearchTable.module.css';
import PropTypes from 'prop-types';
import GameCard from '../GameCard';
import { Link } from 'react-router-dom';
import API from '../../api/apiClient';

function SearchTable(props) {
    const [games, setGames] = useState([]);

    useEffect(() => {
        API.getUserInfo()
            .then((data) => data?.json())
            .then((body) => {
                console.log(body);
                const games = props.games.filter((game) => {
                    return body.games?.some(
                        (userGame) => userGame.id === game.id
                    );
                });

                setGames(games);
            });
    }, [props.games]);

    return (
        <div className={styles.SearchTable}>
            {games.map((game) => {
                return (
                    <Link key={uuid()} to={'/search/' + game.id}>
                        <GameCard
                            gameTitle={game.title}
                            imageLarge={game.imageLarge}
                            icon={game.icon}
                            smallImage={game.imageSmall}
                        />
                    </Link>
                );
            })}
        </div>
    );
}

SearchTable.propTypes = {
    games: PropTypes.array,
};

const mapStateToProps = (state) => {
    return {
        games: state.games,
    };
};

export default connect(mapStateToProps)(SearchTable);
