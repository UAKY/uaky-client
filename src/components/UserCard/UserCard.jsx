import React from 'react';
import styles from './UserCard.module.css';
import PropTypes from 'prop-types';
import UserAvatar from '../UserAvatar';

export default function UserCard({ user }) {
    return (
        <div className={styles.UserCard}>
            <UserAvatar avatar={user?.image} />
            <span className={styles.nickname}>{user?.nickname}</span>
            <span className={styles.desc}>
                Description: {user?.description}
            </span>
        </div>
    );
}

UserCard.propTypes = {
    user: PropTypes.object,
    onChange: PropTypes.func,
};
