import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledUserAvatar = styled.div`
    background-image: url(${(props) => props.avatar});
    width: 200px;
    min-width: 200px;
    height: 200px;
    border-radius: 50%;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    border: 10px solid #1d2633;
    position: relative;
`;

export default function UserAvatar({ avatar, children }) {
    return <StyledUserAvatar avatar={avatar}>{children}</StyledUserAvatar>;
}

UserAvatar.propTypes = {
    avatar: PropTypes.string,
    children: PropTypes.any,
};
