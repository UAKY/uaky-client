import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import API from '../../api/apiClient';
import UserDescription from '../UserDescription';
import UserGames from '../UserGames';

const StyledProfile = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    min-height: 100vh;
    align-items: center;
`;

export default function Profile() {
    const [userGames, setUserGames] = useState([]);
    const [nickname, setNickname] = useState();
    const [avatar, setAvatar] = useState();

    useEffect(() => {
        API.getUserInfo()
            .then((data) => data.json())
            .then((body) => {
                setUserGames(body.games);
                setNickname(body.nickname);
                setAvatar(body.image);
            });
    }, []);

    return (
        <StyledProfile>
            <UserDescription nickname={nickname} avatar={avatar} />
            <UserGames userGames={userGames} />
        </StyledProfile>
    );
}
