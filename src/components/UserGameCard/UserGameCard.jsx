import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Input } from '../../commonStyles/input';
import API from '../../api/apiClient';
import { useHistory } from 'react-router';

const StyledUserGameCard = styled.div`
    transition: 0.4s;
    margin: 15px;
    width: 250px;
    height: 400px;
    background-color: #1d2633;
    font-size: 22px;

    &:hover {
        -webkit-box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
    }
`;

const GameImage = styled.div`
    position: relative;
    background-image: url(${(props) => props.image});
    background-size: cover;
    width: 100%;
    height: 350px;
`;

const GameIcon = styled.div`
    background-image: url(${(props) => props.icon});
    background-size: cover;
    height: 30px;
    width: 30px;
    margin-right: 5px;
`;

const GameTitle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    text-align: center;
    font-weight: bold;
    margin-top: 10px;
`;

const GameDescription = styled.div`
    opacity: 0;
    transition: 0.4s;
    padding: 10px;
    position: absolute;
    width: calc(100% - 20px);
    height: calc(100% - 20px);
    background-color: #000000a4;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    &:hover {
        opacity: 1;
    }

    @media screen and (max-width: 1200px) {
        opacity: 1;
    }
`;

const ProfileLink = styled.label`
    color: white;
`;

const ProfileDescription = styled.label`
    color: white;
`;

const ProfileLinkInput = styled(Input)`
    font-size: inherit;
    width: 100%;
    box-sizing: border-box;
`;

const ProfileDescriptionInput = styled(ProfileLinkInput)``;

const SaveButton = styled.button`
    width: 100%;
    font-size: inherit;
    margin-top: 10px;
`;

const DeleteButton = styled.button`
    width: 100%;
    font-size: inherit;
    margin-top: 10px;
`;

export default function UserGameCard({ userGame, game }) {
    const history = useHistory();

    const updateGame = () => {
        const profileLink = document.getElementById(userGame.id + 'link').value;
        const description = document.getElementById(userGame.id + 'desc').value;

        const data = {
            profileLink: profileLink,
            description: description,
        };

        API.updateUserGame(userGame.id, data);
    };

    const deleteGame = () => {
        API.deleteUserGame(userGame.id).then(() => {
            history.go(0);
        });
    };

    return (
        <StyledUserGameCard>
            <GameImage image={game?.imageLarge}>
                <GameDescription>
                    <ProfileLink htmlFor={userGame.id}>
                        Profile link:
                    </ProfileLink>
                    <ProfileLinkInput
                        id={userGame.id + 'link'}
                        defaultValue={userGame.profileLink}
                    />
                    <ProfileDescription htmlFor={userGame.id + 'desc'}>
                        Description:
                    </ProfileDescription>
                    <ProfileDescriptionInput
                        id={userGame.id + 'desc'}
                        defaultValue={userGame.description}
                    />
                    <SaveButton
                        onClick={updateGame}
                        className="btn-transparent"
                    >
                        Save
                    </SaveButton>
                    <DeleteButton
                        onClick={deleteGame}
                        className="btn-transparent-danger"
                    >
                        Delete
                    </DeleteButton>
                </GameDescription>
            </GameImage>

            <GameTitle>
                <GameIcon icon={game?.icon} />
                {game?.title}
            </GameTitle>
        </StyledUserGameCard>
    );
}

UserGameCard.propTypes = {
    game: PropTypes.object,
    userGame: PropTypes.object,
};
