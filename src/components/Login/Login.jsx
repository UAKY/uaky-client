import React, { useRef, useState } from 'react';
import styles from './Login.module.css';
import API from '../../api/apiClient';
import Cookies from 'js-cookie';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { v4 as uuid } from 'uuid';

function Login(props) {
    const email = useRef();
    const password = useRef();
    const [errors, setErrors] = useState({});

    const prepareData = () => {
        const data = {
            email: email.current.value,
            password: password.current.value,
        };

        return data;
    };

    const onLogin = (e) => {
        e.preventDefault();
        const data = prepareData();
        API.login(data).then((res) => {
            if (res.status === 200) {
                res.json().then((data) => {
                    Cookies.set('accessToken', data.accessToken);
                    Cookies.set('refreshToken', data.refreshToken);
                    props.history.go('/home');
                });
            } else {
                res.json().then((error) => {
                    setErrors({
                        message: [error.message],
                    });
                });
            }
        });
    };

    return (
        <div className={styles.Login}>
            <div className={styles.titleContainer}>
                <h1 className={styles.loginTitle}>
                    Sign in and{' '}
                    <span className={styles.loginTitleStyled}>
                        start searching for a team
                    </span>
                </h1>
                <h2 className={styles.loginDescription}>
                    SKA connects you with other gamers and allows you to search
                    teammates
                </h2>
            </div>
            <form className={styles.form}>
                <label>Email</label>
                <input ref={email} placeholder="Email" />
                <label>Password</label>
                <input type="password" ref={password} placeholder="Password" />
                <div className={styles.errors}>
                    {errors.message?.map((error) => (
                        <span key={uuid()}>{error}</span>
                    ))}
                </div>
                <button
                    className={styles.loginButton + ' btn-transparent'}
                    onClick={onLogin}
                >
                    Login
                </button>
            </form>
        </div>
    );
}

Login.propTypes = {
    history: PropTypes.any,
};

export default withRouter(Login);
