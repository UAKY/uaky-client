import React from 'react';
import Popup from 'reactjs-popup';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ModalContent = styled.div`
    width: 400px;
    height: auto;
    background-color: #1d2633;
    border: 6px solid #243754;
    padding: 10px;

    @media screen and (max-width: 430px) {
        width: calc(100vw - 40px);
    }
`;

const OKButton = styled.button`
    font-size: inherit;
    width: 100%;
    padding: 14px;
`;

const overlayStyle = { background: 'rgba(0,0,0,0.5)' };

const Modal = ({ modalButton, Content, okButtonText, okclick }) => {
    return (
        <Popup
            overlayStyle={overlayStyle}
            trigger={modalButton}
            position="center"
            modal
            lockScroll
        >
            {(close) => (
                <ModalContent>
                    {Content}
                    <OKButton
                        className="btn-transparent"
                        onClick={okclick ? okclick : close}
                    >
                        {okButtonText}
                    </OKButton>
                </ModalContent>
            )}
        </Popup>
    );
};

Modal.propTypes = {
    modalButton: PropTypes.any,
    Content: PropTypes.any,
    okButtonText: PropTypes.string,
    okclick: PropTypes.func,
};

export default Modal;
