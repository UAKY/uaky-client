import React from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import commonBG from '../../assets/images/common-search.png';
import onlineBG from '../../assets/images/online-search.png';
import styled from 'styled-components';

const StyledChoose = styled.div`
    width: 100%;
    height: 100vh;
    text-align: center;
    color: white;
`;

const Search = styled.div`
    cursor: pointer;
    transition: 0.4s;
    width: 45%;
    height: 60vh;
    border: 10px solid #1d2633;
    background-size: 200%;
    animation: AnimationColapse 90s ease infinite;

    @keyframes AnimationColapse {
        0% {
            background-position: 0% 50%;
        }
        50% {
            background-position: 100% 50%;
        }
        100% {
            background-position: 0% 50%;
        }
    }

    &:hover {
        -webkit-box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
    }

    @media screen and (max-width: 1200px) {
        height: 50vh;
    }

    @media screen and (max-width: 950px) {
        height: 30vh;
        width: 65%;
        margin: 10px;
    }

    @media screen and (max-width: 580px) {
        width: 95%;
    }
`;

const CommonSearch = styled(Search)`
    background-image: url(${commonBG});
`;

const OnlineSearch = styled(Search)`
    background-image: url(${onlineBG});
`;

const Searches = styled.div`
    margin-top: 60px;
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;

    @media screen and (max-width: 950px) {
        flex-direction: column;
    }
`;

const Shadow = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #1d2633;
    opacity: 0.8;
`;

const Text = styled.div`
    font-size: 50px;
    font-weight: bold;
    border: 2px solid #94ae3f;
    padding: 120px;

    @media screen and (max-width: 1200px) {
        padding: 70px;
        font-size: 40px;
    }

    @media screen and (max-width: 950px) {
        font-size: 50px;
        padding: 50px;
    }

    @media screen and (max-width: 580px) {
        font-size: 40px;
        padding: 10px;
    }
`;

function SearchChoose() {
    const location = useLocation();
    const history = useHistory();

    return (
        <StyledChoose>
            <h1>Choose search</h1>
            <Searches>
                <CommonSearch
                    onClick={() => history.push(location.pathname + '/common')}
                >
                    <Shadow>
                        <Text>Common</Text>
                    </Shadow>
                </CommonSearch>
                <OnlineSearch
                    onClick={() => history.push(location.pathname + '/online')}
                >
                    <Shadow>
                        <Text>Online</Text>
                    </Shadow>
                </OnlineSearch>
            </Searches>
        </StyledChoose>
    );
}

export default SearchChoose;
