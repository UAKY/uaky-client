import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import API from '../../api/apiClient';
import { v4 as uuid } from 'uuid';
import Modal from '../Modal';
import PropTypes from 'prop-types';

const StyledUserLikes = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    min-height: 100vh;
    align-items: center;
    justify-content: flex-start;
`;

const Title = styled.h1`
    color: white;
    margin-top: 30px;
`;

const UserLike = styled.div`
    position: relative;
    width: 80%;
    height: 70px;
    background-color: #243754;
    border: 5px solid #1d2633;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 20px 60px;
    margin: 10px;
    overflow: hidden;

    @media screen and (max-width: 750px) {
        flex-direction: column;
        height: auto;
        width: 80%;
        padding: 5px 20px 20px 20px;
        -webkit-box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 14px 21px 0px rgba(0, 0, 0, 0.2);
    }
`;

const UserSmallAvatar = styled.div`
    border: 5px solid #1d2633;
    border-radius: 50%;
    min-width: 60px;
    height: 60px;
    background-image: url(${(props) => props.image});
    background-size: cover;
    margin: 0 auto;

    @media screen and (max-width: 750px) {
        display: none;
    }
`;

const UserNickname = styled.span`
    padding-bottom: 10px;
    margin: 20px;
    font-size: 18px;
    color: white;
    font-weight: bold;
    border-bottom: 2px solid #94ae3f;
    z-index: 3;
`;

const Info = styled.div`
    display: flex;
    align-items: center;
`;

const MoreButton = styled.button`
    padding: 12px;
    font-size: 18px;
    z-index: 3;
`;

const StyledModalContent = styled.div`
    display: flex;
    flex-direction: column;
    padding: 20px 20px 40px 20px;
`;

const Text = styled.span`
    text-align: justify;
    color: white;
    font-size: 18px;
    margin: 10px 0;
    word-wrap: break-word;
`;

const Heading = styled.span`
    font-size: 18px;
    color: #94ae3f;
    font-weight: bold;
    margin-top: 5px;
`;

const SimpleUserNickname = styled(UserNickname)`
    font-size: 22px;
    margin: 10px 0;
    text-align: center;
`;

const Img = styled.div`
    opacity: 0.7;
    z-index: 1;
    width: 450px;
    height: 580px;
    position: absolute;
    top: 50%;
    left: 54%;
    transform: translate(-50%, -50%) rotate(-5deg);

    @media screen and (max-width: 750px) {
        opacity: 0.4;
    }
`;

const InnerImage = styled.div`
    background-image: url(${(props) => props.image});
    background-size: cover;
    width: 100%;
    height: 100%;
    position: relative;

    &:after {
        content: '';
        width: 110%;
        height: 110%;
        position: absolute;
        background: linear-gradient(
            90deg,
            rgba(36, 55, 84, 1) 5%,
            rgba(36, 55, 84, 0.8) 27%,
            rgba(0, 67, 112, 0) 45%,
            rgba(0, 67, 112, 0) 55%,
            rgba(36, 55, 84, 0.8) 76%,
            rgba(36, 55, 84, 1) 95%
        );
        top: -5px;
        left: -5px;
    }
`;

export default function UserLikes() {
    const [likes, setLikes] = useState([]);

    const Content = ({ like }) => (
        <StyledModalContent>
            <UserSmallAvatar image={like.userImage} />
            <SimpleUserNickname>{like.nickname}</SimpleUserNickname>
            <Heading>Profile link:</Heading>
            <Text>{like.profileLink}</Text>
            <Heading>Description:</Heading>
            <Text>{like.description}</Text>
        </StyledModalContent>
    );

    Content.propTypes = {
        like: PropTypes.any,
    };

    useEffect(() => {
        API.getUserLikes()
            .then((data) => data.json())
            .then((body) => {
                setLikes(body);
            });
    }, []);

    return (
        <StyledUserLikes>
            <Title>There you can see, who like you</Title>
            {likes.map((like) => {
                return (
                    <UserLike key={uuid()}>
                        <Info>
                            <UserSmallAvatar image={like.userImage} />
                            <UserNickname>
                                {like.nickname} likes you in {like.gameTitle}
                            </UserNickname>
                        </Info>

                        <Img>
                            <InnerImage image={like.gameLargeImage} />
                        </Img>

                        <Modal
                            modalButton={
                                <MoreButton className="btn-transparent">
                                    Show more information
                                </MoreButton>
                            }
                            Content={<Content like={like} />}
                            okButtonText="OK"
                        />
                    </UserLike>
                );
            })}
        </StyledUserLikes>
    );
}
