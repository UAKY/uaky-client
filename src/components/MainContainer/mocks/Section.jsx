import React from 'react';

function Section() {
    return (
        <section
            style={{
                width: '100%',
                backgroundColor: '#243754',
                height: '500px',
                marginBottom: '20px',
            }}
        ></section>
    );
}

export default Section;
