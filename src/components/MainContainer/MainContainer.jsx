import React from 'react';
import styles from './MainContainer.module.css';
import PropTypes from 'prop-types';

function MainContainer({ bg, children, isCentered = true }) {
    return (
        <main
            className={styles.MainContainer}
            style={{ background: `url(${bg})` }}
        >
            <div
                className={styles.content}
                style={{ alignItems: isCentered ? 'center' : 'flex-start' }}
            >
                {children}
            </div>
        </main>
    );
}

MainContainer.propTypes = {
    bg: PropTypes.any,
    children: PropTypes.element,
    isCentered: PropTypes.bool,
};

export default MainContainer;
