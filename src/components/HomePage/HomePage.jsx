import React from 'react';
import styled from 'styled-components';

const StyledHomePage = styled.div`
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    height: 80vh;
    margin-top: -150px;
    width: 100%;

    @media screen and (max-width: 595px) {
        height: 70vh;
        margin-top: -250px;
        justify-content: flex-start;
    }
`;

const Text = styled.h1`
    text-align: left;
    font-size: 90px;
    color: white;
    font-weight: lighter;

    @media screen and (max-width: 595px) {
        font-size: 50px;
        text-align: center;
    }
`;

const StyledText = styled.h1`
    font-size: 90px;
    color: #94ae3f;
    text-align: right;
    font-weight: bold;

    @media screen and (max-width: 595px) {
        font-size: 40px;
        text-align: center;
    }
`;

export default function HomePage() {
    return (
        <StyledHomePage>
            <Text>Start looking</Text>
            <StyledText>Keep playing</StyledText>
        </StyledHomePage>
    );
}
