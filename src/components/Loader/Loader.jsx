import styles from './Loader.module.css';
import React from 'react';

function Loader() {
    return (
        <div className={styles.ldsGrid}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
}

export default Loader;
