import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import UserCard from '../UserCard/UserCard';
import { loadOnlineData } from './connectStore';
import PropTypes from 'prop-types';
import { useParams } from 'react-router';
import { v4 as uuid } from 'uuid';
import API from '../../api/apiClient';
import TinderCard from '../../staticLibs/react-tinder-card';
import styled from 'styled-components';

const StyledSearch = styled.div`
    margin-top: -200px;
    background-color: transparent;
    border: 5px solid #1d2633;
    position: relative;
    width: 300px;
    height: 500px;

    @media screen and (max-width: 360px) {
        width: 80vw;
        height: 400px;
        margin-top: -100px;
    }
`;

const SearchCard = styled.div`
    position: absolute;
    top: -20px;
    left: -20px;
    user-select: none;

    @media screen and (max-width: 360px) {
        left: 50%;
        transform: translateX(-50%);
        top: -20px;
    }
`;

const Button = styled.div`
    color: white;
    font-size: 20px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    cursor: pointer;
    background-color: transparent;
    transition: 0.4s;

    &:hover {
        background-color: #1d2633;
    }
`;

const I = styled.i`
    margin-bottom: 20px;
    font-size: 90px;
    color: #94ae3f;
`;

function Search(props) {
    const [isRefreshButtonVisible, setIsRefreshButtonVisible] = useState(false);
    const params = useParams();

    const refresh = () => {
        API.onlineRefreshUsers(params.id).then(() => {
            setIsRefreshButtonVisible(false);
            getData();
        });
    };

    useEffect(() => {
        refresh();
    }, []);

    const onSwipe = (direction, userId, gameId) => {
        if (direction === 'right') {
            API.likeUser(userId, gameId);
        }
    };

    const onCardLeftScreen = () => {
        getData().then((data) => {
            if (data.content.length === 0) {
                setIsRefreshButtonVisible(true);
            }
        });
    };

    const getData = () => {
        return props.loadOnlineData(params.id);
    };

    return (
        <StyledSearch>
            {props?.users?.map((user, id) => {
                let onCardSwipe = () => {};
                if (id === 0) onCardSwipe = () => onCardLeftScreen();
                return (
                    <SearchCard key={uuid()}>
                        <TinderCard
                            onSwipe={(direction) =>
                                onSwipe(direction, user.id, params.id)
                            }
                            onCardLeftScreen={onCardSwipe}
                            preventSwipe={['up', 'down']}
                        >
                            <UserCard user={user} />
                        </TinderCard>
                    </SearchCard>
                );
            })}

            {isRefreshButtonVisible && (
                <Button onClick={refresh}>
                    <I className="fas fa-redo-alt"></I>Refresh
                </Button>
            )}
        </StyledSearch>
    );
}

Search.propTypes = {
    loadOnlineData: PropTypes.func,
    users: PropTypes.array,
};

const mapStateToProps = (state) => {
    return {
        users: state.users,
    };
};

const mapDispatchToProps = {
    loadOnlineData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
