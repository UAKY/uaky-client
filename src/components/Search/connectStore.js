import API from '../../api/apiClient';
import { loadUsers } from '../../redux/actionsCreators';

export const loadData = (id) => {
    return (dispatch) => {
        return API.commonSearchUsers(id).then((res) => {
            return res.json().then((users) => {
                dispatch(loadUsers(users.content));
                return users;
            });
        });
    };
};

export const loadOnlineData = (id) => {
    return (dispatch) => {
        return API.onlineSearchUsers(id).then((res) => {
            return res.json().then((users) => {
                dispatch(loadUsers(users.content));
                return users;
            });
        });
    };
};
