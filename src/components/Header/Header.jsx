import React, { useRef, useState } from 'react';
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import styles from './Header.module.css';

import Logo from '../Logo';
import { Link } from 'react-router-dom';

function Header(props) {
    const [height, setHeight] = useState('0');
    const [bgColor, setBgColor] = useState('#1d2633');
    const [padding, setPadding] = useState('0');
    const [openClass, setOpenClass] = useState('');

    const hamburger = useRef();

    const handleClick = () => {
        if (hamburger.current.clientHeight === 0) return;
        if (height === '0') {
            setOpenClass(styles.open);
            setHeight('250px');
            setPadding('20px');
            setBgColor('#273342');
            return;
        }
        setOpenClass('');
        setHeight('0');
        setPadding('0');
        setBgColor('#1d2633');
    };

    const logOut = () => {
        handleClick();
        Cookies.remove('accessToken');
        Cookies.remove('refreshToken');
        props.history.push('/');
    };

    return (
        <header className={styles.Header}>
            <div
                className={styles.headerContainer}
                style={{ backgroundColor: bgColor }}
            >
                <div className={styles.logoContainer}>
                    <Logo size="50px" />

                    <div
                        onClick={handleClick}
                        className={styles.hamb + ' ' + openClass}
                        ref={hamburger}
                    >
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div
                    className={styles.navBar}
                    style={{ maxHeight: height, paddingBottom: padding }}
                >
                    <div className={styles.navLinks}>
                        <div className={styles.hambTitle}>
                            <i className="fas fa-sitemap"></i> Site routes
                        </div>
                        <div
                            className={styles.navElement}
                            onClick={handleClick}
                        >
                            <Link to="/home">Home</Link>
                        </div>
                        {Cookies.get('accessToken') && (
                            <div
                                className={styles.navElement}
                                onClick={handleClick}
                            >
                                <Link to="/search">Search teammates</Link>
                            </div>
                        )}
                    </div>
                    <div className={styles.loginBar}>
                        <div className={styles.hambTitle}>
                            <i className="fas fa-user-circle"></i> Account
                        </div>
                        {!Cookies.get('accessToken') && (
                            <div
                                className={styles.navElement}
                                onClick={handleClick}
                            >
                                <Link to="/login">Login</Link>
                            </div>
                        )}
                        {!Cookies.get('accessToken') && (
                            <div
                                className={styles.navElement}
                                onClick={handleClick}
                            >
                                <Link to="/registration">Sign up</Link>
                            </div>
                        )}
                        {Cookies.get('accessToken') && (
                            <>
                                <div
                                    className={styles.navElement}
                                    onClick={handleClick}
                                >
                                    <Link to="/likes">My likes</Link>
                                </div>
                                <div
                                    className={styles.navElement}
                                    onClick={handleClick}
                                >
                                    <Link to="/me">Profile</Link>
                                </div>
                                <div
                                    className={styles.navElement}
                                    onClick={logOut}
                                >
                                    Logout
                                </div>
                            </>
                        )}
                    </div>
                </div>
            </div>
        </header>
    );
}

Header.propTypes = {
    history: PropTypes.any,
};

export default withRouter(Header);
