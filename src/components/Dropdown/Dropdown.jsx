import React, { useRef, useState } from 'react';
import styles from './Dropdown.module.css';
import PropTypes from 'prop-types';
import { v4 as uuid } from 'uuid';

export default function Dropdown({ selectId, choices, dropdownPlaceholder }) {
    const [selected, setSelected] = useState(dropdownPlaceholder);
    const [overflow, setOverflow] = useState('hidden');
    const options = useRef();
    const arrow = useRef();

    const onClickOption = (e) => {
        arrow.current.style.transform = 'rotate(0deg)';
        arrow.current.style.color = 'white';
        selectId(+e.target.getAttribute('keyid'));
        setSelected(e.target.innerText);
        setOverflow('hidden');
    };

    const onFocus = () => {
        if (overflow === 'hidden') {
            setOverflow('visible');
            arrow.current.style.transform = 'rotate(180deg)';
            arrow.current.style.color = '#94ae3f';
        } else {
            setOverflow('hidden');
            arrow.current.style.transform = 'rotate(0deg)';
            arrow.current.style.color = 'white';
        }
    };

    const onBlur = () => {
        arrow.current.style.transform = 'rotate(0deg)';
        arrow.current.style.color = 'white';
        setOverflow('hidden');
    };

    const onClick = () => {
        arrow.current.style.transform = 'rotate(180deg)';
        arrow.current.style.color = '#94ae3f';
        setOverflow('visible');
    };

    return (
        <div
            tabIndex="0"
            onFocus={onFocus}
            onBlur={onBlur}
            style={{ overflow: overflow }}
            className={styles.Dropdown}
        >
            <div onClick={onClick} className={styles.dropdownHeader}>
                <div className={styles.dropdownTitle}>{selected}</div>
                <div ref={arrow} className={styles.dropdownArrow}>
                    <i className="fas fa-chevron-down"></i>
                </div>
            </div>
            <div ref={options} className={styles.dropdownOptions}>
                {choices.map((choice, index) => (
                    <div
                        className={styles.dropdownOption}
                        key={uuid()}
                        keyid={index}
                        onClick={onClickOption}
                        style={{ backgroundImage: `url(${choice.imageSmall})` }}
                    >
                        {choice.title}
                    </div>
                ))}
            </div>
        </div>
    );
}

Dropdown.propTypes = {
    selectId: PropTypes.func,
    choices: PropTypes.array,
    dropdownPlaceholder: PropTypes.string,
};
