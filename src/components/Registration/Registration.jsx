import React, { useRef, useState } from 'react';
import styles from './Registration.module.css';
import Dropdown from '../Dropdown';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import API from '../../api/apiClient';
import { v4 as uuid } from 'uuid';
import { useHistory } from 'react-router';

function Registration(props) {
    const dropdownPlaceholder = 'Select game';
    const [selectedGame, setSelectedGame] = useState(dropdownPlaceholder);
    const [addedGames, setAddedGames] = useState([]);
    const link = useRef();
    const desc = useRef();
    const emailInput = useRef();
    const passwordInput = useRef();
    const confirmPasswordInput = useRef();
    const nicknameInput = useRef();
    const history = useHistory();
    const [errors, setErrors] = useState({});

    const register = (e) => {
        e.preventDefault();
        const data = prepareData();
        if (data) {
            API.registration(data).then((data) => {
                if (data.status === 204) {
                    history.push('/login');
                } else {
                    data.json().then((err) => {
                        setErrors(err.validationErrors);
                    });
                }
            });
        }
    };

    const prepareData = () => {
        const email = emailInput.current.value;
        const password = passwordInput.current.value;
        const confirmPassword = confirmPasswordInput.current.value;

        if (password !== confirmPassword) {
            setErrors({
                ...errors,
                confirm: ['password is not equal'],
            });
            return;
        }

        const nickname = nicknameInput.current.value;
        const games = addedGames.map((game) => {
            const { id, profileLink, description } = game;
            return {
                id,
                profileLink,
                description,
            };
        });

        return {
            email,
            password,
            nickname,
            games,
        };
    };

    const showId = (id) => {
        setSelectedGame(id);
    };

    const deleteGame = (e) => {
        const newGames = addedGames.filter(
            (game) => game.title !== e.target.parentNode.innerText
        );
        setAddedGames(newGames);
    };

    const addGame = (e) => {
        e.preventDefault();
        const games = [...addedGames];
        let isGameAlreadyAdded = false;
        games.forEach((game) => {
            if (game.title === props.games[selectedGame].title)
                isGameAlreadyAdded = true;
        });
        if (!isGameAlreadyAdded) {
            if (!link.current.value || !desc.current.value) {
                if (!link.current.value) {
                    setErrors({
                        ...errors,
                        link: ['this field should not be empty'],
                    });
                }

                if (!desc.current.value) {
                    setErrors({
                        ...errors,
                        desc: ['this field should not be empty'],
                    });
                }

                return;
            }

            setErrors({
                ...errors,
                link: [],
                desc: [],
            });

            games.push({
                id: props.games[selectedGame].id,
                icon: props.games[selectedGame].icon,
                title: props.games[selectedGame].title,
                profileLink: link.current.value,
                description: desc.current.value,
            });
            setSelectedGame(dropdownPlaceholder);
            setAddedGames(games);
            setErrors({
                ...errors,
                games: [],
            });
        } else {
            setErrors({
                ...errors,
                games: ['You have added this game yet'],
            });
        }
    };

    return (
        <div className={styles.Registration}>
            <div className={styles.titleContainer}>
                <h1 className={styles.registerTitle}>
                    Create an{' '}
                    <span className={styles.registerTitleStyled}>account</span>
                </h1>
                <h2 className={styles.registerDescription}>
                    SKA connects you with other gamers and allows you to search
                    new teammates
                </h2>
            </div>
            <form className={styles.form}>
                <label>Email</label>
                <input ref={emailInput} placeholder="Email" />
                <div className={styles.errors}>
                    {errors.email?.map((error) => (
                        <span key={uuid()}>{error}</span>
                    ))}
                </div>
                <label>Password</label>
                <input
                    type="password"
                    ref={passwordInput}
                    placeholder="Password"
                />
                <div className={styles.errors}>
                    {errors.password?.map((error) => (
                        <span key={uuid()}>{error}</span>
                    ))}
                </div>
                <label>Confirm Password</label>
                <input
                    type="password"
                    ref={confirmPasswordInput}
                    placeholder="Confirm Password"
                />
                <div className={styles.errors}>
                    {errors.confirm?.map((error) => (
                        <span key={uuid()}>{error}</span>
                    ))}
                </div>
                <label>Nickname</label>
                <input ref={nicknameInput} placeholder="Nickname" />
                <div className={styles.errors}>
                    {errors.nickname?.map((error) => (
                        <span key={uuid()}>{error}</span>
                    ))}
                </div>
                <label>Games</label>
                <Dropdown
                    selectId={showId}
                    choices={props.games}
                    dropdownPlaceholder={dropdownPlaceholder}
                />
                <div className={styles.errors}>
                    {errors.games?.map((error) => (
                        <span key={uuid()}>{error}</span>
                    ))}
                </div>
                <div className={styles.addedGames}>
                    {addedGames.map((game) => (
                        <div className={styles.addedGame} key={uuid()}>
                            <div
                                className={styles.gameIcon}
                                style={{ backgroundImage: `url(${game.icon})` }}
                            />
                            {game.title}
                            <i
                                onClick={deleteGame}
                                className="fas fa-times"
                            ></i>
                        </div>
                    ))}
                </div>
                {selectedGame !== dropdownPlaceholder && (
                    <div className={styles.addGame}>
                        <label>Profile link</label>
                        <input ref={link} placeholder="Profile link" />
                        <div className={styles.errors}>
                            {errors.link?.map((error) => (
                                <span key={uuid()}>{error}</span>
                            ))}
                        </div>
                        <label>Description</label>
                        <input ref={desc} placeholder="Description" />
                        <div className={styles.errors}>
                            {errors.desc?.map((error) => (
                                <span key={uuid()}>{error}</span>
                            ))}
                        </div>
                        <button
                            className={
                                styles.addGameButton + ' btn-transparent'
                            }
                            onClick={addGame}
                        >
                            Add game
                        </button>
                    </div>
                )}
                <button
                    className={styles.signupButton + ' btn-transparent'}
                    onClick={register}
                >
                    Sign up
                </button>
            </form>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        games: state.games,
    };
};

Registration.propTypes = {
    games: PropTypes.array,
};

export default connect(mapStateToProps)(Registration);
