import React, { useState } from 'react';
import styled from 'styled-components';
import UserAvatar from '../UserAvatar';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import { Input, Error } from '../../commonStyles/input';
import API from '../../api/apiClient';
import ChangeAvatar from '../ChangeAvatar';
import Cookies from 'js-cookie';
import { useHistory } from 'react-router';

const Wrapper = styled.section`
    width: 80%;
    display: flex;
    flex-wrap: wrap;
    padding: 10px;
    padding-bottom: 40px;
    justify-content: center;
    align-items: flex-start;
    border-bottom: 1px solid #94ae3f;
`;

const Description = styled.div`
    width: 700px;
    text-align: justify;
    display: flex;
    flex-direction: column;
    padding: 0 20px;
`;

const UserName = styled.h1`
    color: white;
    font-weight: lighter;
    font-size: 28px;
    border-bottom: 1px solid #94ae3f;

    @media screen and (max-width: 900px) {
        text-align: center;
    }
`;

const UserInformation = styled.h2`
    display: flex;
    align-items: center;
    justify-content: center;

    @media screen and (max-width: 900px) {
        flex-direction: column;
    }
`;

const Button = styled.button`
    font-size: 16px;
    width: auto;
    padding: 14px;
    margin-right: 20px;
    margin-bottom: 10px;

    @media screen and (max-width: 900px) {
        margin-right: 0;
    }
`;

const FormInput = styled(Input)`
    width: calc(100% - 40px);
    font-size: inherit;
    margin-bottom: 20px;
`;

const Label = styled.label`
    font-size: inherit;
    color: white;
    margin-bottom: 5px;
`;

export default function UserDescription({ nickname, avatar }) {
    const [errors, setErrors] = useState({});
    const history = useHistory();

    const handleFileChange = (event) => {
        if (event.target.files && event.target.files.length > 0) {
            const file = event.target.files[0];

            if (!file) {
                alert('Select image first.');
                return false;
            }

            const formData = new FormData();
            formData.append('file', file);

            API.uploadPicture(formData).then((data) => {
                if (data?.status === 200) {
                    data.json().then((body) => {
                        API.editUserPicture({ image: body.imageLink }).then(
                            (data) => {
                                if (data?.status === 204) {
                                    history.go(0);
                                }
                            }
                        );
                    });
                }
            });
        }
    };

    const ContentForNickname = (
        <>
            <Label htmlFor="changeNickname">Nickname:</Label>
            <FormInput id="changeNickname" defaultValue={nickname} />
            <Error errors={errors.nick} />
        </>
    );

    const ContentForPassword = (
        <>
            <Label htmlFor="oldpass">Old Password:</Label>
            <FormInput id="oldpass" type="password" />
            <Label htmlFor="newpass">New Password:</Label>
            <FormInput id="newpass" type="password" />
            <Label htmlFor="confnewpass">Confirm New Password:</Label>
            <FormInput id="confnewpass" type="password" />
        </>
    );

    const changeNickname = () => {
        const nickname = document.getElementById('changeNickname').value;

        const data = {
            nickname,
        };

        API.editNickname(data).then((res) => {
            if (res.status === 204) {
                history.go(0);
            } else {
                res.json().then((err) => {
                    setErrors({
                        nick: err.validationErrors.nickname,
                    });
                });
            }
        });
    };

    const changePassword = () => {
        const oldPassword = document.getElementById('oldpass').value;
        const newPassword = document.getElementById('newpass').value;
        const repeatedNewPassword = document.getElementById('confnewpass')
            .value;

        const data = {
            oldPassword,
            newPassword,
            repeatedNewPassword,
        };

        API.editPassword(data).then((res) => {
            if (res.status === 204) {
                Cookies.remove('accessToken');
                Cookies.remove('refreshToken');
                history.push('/login');
            }
        });
    };

    return (
        <Wrapper>
            <UserAvatar avatar={avatar}>
                <ChangeAvatar id="uploadPicture" />
                <input
                    id="uploadPicture"
                    type="file"
                    style={{ display: 'none' }}
                    onChange={handleFileChange}
                />
            </UserAvatar>
            <Description>
                <UserName>{nickname}</UserName>
                <UserInformation>
                    <Modal
                        modalButton={
                            <Button className="btn-transparent">
                                Change nickname
                            </Button>
                        }
                        Content={ContentForNickname}
                        okButtonText="Save"
                        okclick={changeNickname}
                    />
                    <Modal
                        modalButton={
                            <Button className="btn-transparent">
                                Change password
                            </Button>
                        }
                        Content={ContentForPassword}
                        okButtonText="Save"
                        okclick={changePassword}
                    />
                </UserInformation>
            </Description>
        </Wrapper>
    );
}

UserDescription.propTypes = {
    nickname: PropTypes.string,
    avatar: PropTypes.string,
};
